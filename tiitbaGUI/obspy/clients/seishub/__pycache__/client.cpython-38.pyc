U
    ��	a)�  �                   @   s�  d Z ddlmZmZmZmZ ddlT ddlmZm	Z	 ddl
Z
ddlZddlZddlZddlZddlmZ ddlmZ ejjdkr�dd	lmZ ddlZndd	lmZ ddlmZ dd
lmZ ddlmZmZm Z  ddl!m"Z"m#Z#m$Z$ ddl%m&Z& ddl'm(Z( ddl)m*Z* ddl+m,Z, ddgZ-dddgZ.e-e. Z/ddddddd�Z0dd� Z1dd � Z2G d!d"� d"e3�Z4G d#d$� d$e3�Z5G d%d&� d&e3�Z6G d'd(� d(e5�Z7G d)d*� d*e5�Z8G d+d,� d,ej9�Z:e;d-k�r�ddl<Z<e<j=d.d/� dS )0z�
SeisHub database client for ObsPy.

:copyright:
    The ObsPy Development Team (devs@obspy.org)
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
�    )�absolute_import�division�print_function�unicode_literals)�*)�PY2�
native_strN)�datetime)�log�   )�	urlencode)�	objectify)�Element�
SubElement�tostring)�Catalog�UTCDateTime�read_events)�guess_delta)�deprecated_keywords)�Parser)�SEEDParserException�PUTZPOST�HEADZGET�DELETEZ
network_idZ
station_idZlocation_idZ
channel_id�start_datetime�end_datetime)�network�station�location�channel�	starttime�endtimec                 C   s"   t rt�| �}ntj| dd�}|S )Nzlatin-1)�encoding)r   �pickle�loads)�data�obj� r(   �1Lib\site-packages\obspy\clients\seishub\client.py�	_unpickle4   s    r*   c                 C   sV   g }| � � D ]D}i }|j�� D ]&\}}|dkr6|j}n|j}|||< q|�|� q|S )z�
    :type root: :class:`lxml.objectify.ObjectifiedElement`
    :param root: Root node of result set returned by
        :func:`lxml.objectify.fromstring`.
    :rtype: list of dict
    �resource_name)�getchildren�__dict__�items�textZpyval�append)�root�result�nodeZresult_�k�vr(   r(   r)   �_objectify_result_to_dicts?   s    
r6   c                   @   sH   e Zd ZdZddd�Zd	d
� Zdd� Zdd� Zdi fdd�Zdd� Z	dS )�Clienta�  
    SeisHub database request Client class.

    The following classes are automatically linked with initialization.
    Follow the links in "Linked Class" for more information. They register
    via the name listed in "Entry Point".

    ===================  ============================================================
    Entry Point          Linked Class
    ===================  ============================================================
    ``Client.waveform``  :class:`~obspy.clients.seishub.client._WaveformMapperClient`
    ``Client.station``   :class:`~obspy.clients.seishub.client._StationMapperClient`
    ``Client.event``     :class:`~obspy.clients.seishub.client._EventMapperClient`
    ===================  ============================================================

    .. rubric:: Example

    >>> from obspy.clients.seishub import Client
    >>>
    >>> t = UTCDateTime("2009-09-03 00:00:00")
    >>> client = Client(timeout=20)
    >>>
    >>> st = client.waveform.get_waveforms(
    ...     "BW", "RTBE", "", "EHZ", t, t + 20)  # doctest: +SKIP
    >>> print(st)  # doctest: +ELLIPSIS +SKIP
    1 Trace(s) in Stream:
    BW.RTBE..EHZ | 2009-09-03T00:00:00.000000Z - ... | 200.0 Hz, 4001 samples
    �+http://teide.geophysik.uni-muenchen.de:8080�admin�
   F�   c           
      C   s|   || _ t| �| _t| �| _t| �| _|| _|| _|| _	i | _
i | _t�� }|�d|||� t�|�}t�|�}	t�|	� dS )aQ  
        Initializes the SeisHub Web service client.

        :type base_url: str, optional
        :param base_url: SeisHub connection string. Defaults to
            'http://teide.geophysik.uni-muenchen.de:8080'.
        :type user: str, optional
        :param user: The user name used for identification with the Web
            service. Defaults to ``'admin'``.
        :type password: str, optional
        :param password: A password used for authentication with the Web
            service. Defaults to ``'admin'``.
        :type timeout: int, optional
        :param timeout: Seconds before a connection timeout is raised (default
            is 10 seconds).
        :type debug: bool, optional
        :param debug: Enables verbose output.
        :type retries: int
        :param retries: Number of retries for failing requests.
        N)�base_url�_WaveformMapperClientZwaveform�_StationMapperClientr   �_EventMapperClient�event�timeout�debug�retries�	xml_seeds�station_list�urllib_requestZHTTPPasswordMgrWithDefaultRealmZadd_passwordZHTTPBasicAuthHandlerZbuild_openerZinstall_opener)
�selfr<   �userZpasswordrA   rB   rC   Zpassword_mgrZauth_handlerZopenerr(   r(   r)   �__init__u   s    




zClient.__init__c                 C   sJ   z0t � � }tj| j| jd���  t � � | d W S  tk
rD   Y nX dS )z*
        Ping the SeisHub server.
        �rA   g     @�@N)�timerF   �urlopenr<   rA   �read�	Exception)rG   �t1r(   r(   r)   �ping�   s    zClient.pingc                 C   s@   | j | jd dd�\}}|dkr$dS |dkr0dS td| ��d	S )
z�
        Test if authentication information is valid. Raises an Exception if
        status code of response is not 200 (OK) or 401 (Forbidden).

        :rtype: bool
        :return: ``True`` if OK, ``False`` if invalid.
        �/xml/r   )�method��   Ti�  Fz"Unexpected request status code: %sN)�_http_requestr<   rN   )rG   �codeZ_msgr(   r(   r)   �	test_auth�   s    �
zClient.test_authc              	   O   s�  i }t �� D ]&\}}||�� kr|| ||< ||= q|�� D ]�\}}|sR|dkrRq<t|t�r�t|�dkr�t|d �|dt|� < t|d �|dt|� < q<t|t�r�t|�dkr�t|d �|dt|� < t|d �|dt|� < q<t|�|t|�< q<| j| d t	|� }| j
�rtd| � t| j�D ]F}z"tj|| jd�}	|	�� }
|
W   S  tk
�rj   Y �q(Y nX �q(tj|| jd�}	|	�� }
|
S )	Nr   r   Zmin_�   Zmax_�?z
Requesting %srJ   )�KEYWORDSr.   �keys�
isinstance�tuple�len�str�listr<   r   rB   �print�rangerC   rF   rL   rA   rM   rN   )rG   �url�args�kwargsZparams�key�valueZ
remoteaddrZ_i�response�docr(   r(   r)   �_fetch�   s<    �
zClient._fetchNc              
   C   s�   |t krtdt  ��|tkr.|s.td| ��n|tkrF|rFtd| ��t||||d�}ztj|| jd�}|j	|j
fW S  tjk
r� } z|j	|j
f W Y �S d}~X Y nX dS )a�  
        Send a HTTP request via urllib2.

        :type url: str
        :param url: Complete URL of resource
        :type method: str
        :param method: HTTP method of request, e.g. "PUT"
        :type headers: dict
        :param headers: Header information for request, e.g.
                {'User-Agent': "obspyck"}
        :type xml_string: str
        :param xml_string: XML for a send request (PUT/POST)
        zMethod must be one of %szMissing data for %s request.zUnexpected data for %s request.)rR   rb   r&   �headersrJ   N)�HTTP_ACCEPTED_METHODS�
ValueError�HTTP_ACCEPTED_DATA_METHODS�	TypeError�HTTP_ACCEPTED_NODATA_METHODS�_RequestWithMethodrF   rL   rA   rU   �msgZ	HTTPError)rG   rb   rR   �
xml_stringrj   Zreqrg   �er(   r(   r)   rT   �   s     ��zClient._http_requestc                 O   s   | j |f|�|�}t�|�S �N)ri   r   Z
fromstring)rG   rb   rc   rd   rh   r(   r(   r)   �
_objectify   s    zClient._objectify)r8   r9   r9   r:   Fr;   )
�__name__�
__module__�__qualname__�__doc__rI   rP   rV   ri   rT   ru   r(   r(   r(   r)   r7   X   s            �
(' r7   c                   @   s>   e Zd Zdd� Zddd�Zdd� Zi fdd	�Zi fd
d�ZdS )�_BaseRESTClientc                 C   s
   || _ d S rt   ��client�rG   r|   r(   r(   r)   rI     s    z_BaseRESTClient.__init__Nc                 K   sP   t � �� D ]\}}|dkr
|||< q
d| j d | j d | }| jj|f|�S )z�
        Gets a resource.

        :type resource_name: str
        :param resource_name: Name of the resource.
        :type format: str, optional
        :param format: Format string, e.g. ``'xml'`` or ``'map'``.
        :return: Resource
        �rG   rd   rQ   �/)�localsr.   �package�resourcetyper|   ri   )rG   r+   �formatrd   re   rf   rb   r(   r(   r)   �get_resource	  s    
�z_BaseRESTClient.get_resourcec                 K   s,   d| j  d | j d | }| jj|f|�S )z�
        Gets a XML resource.

        :type resource_name: str
        :param resource_name: Name of the resource.
        :return: Resource as :class:`lxml.objectify.ObjectifiedElement`
        rQ   r   )r�   r�   r|   ru   )rG   r+   rd   rb   r(   r(   r)   �get_xml_resource  s    �z _BaseRESTClient.get_xml_resourcec                 C   s0   d� | jjd| j| j|g�}| jj|d||d�S )a�  
        PUTs a XML resource.

        :type resource_name: str
        :param resource_name: Name of the resource.
        :type headers: dict
        :param headers: Header information for request,
            e.g. ``{'User-Agent': "obspyck"}``
        :type xml_string: str
        :param xml_string: XML for a send request (PUT/POST)
        :rtype: tuple
        :return: (HTTP status code, HTTP status message)

        .. rubric:: Example

        >>> c = Client()
        >>> xseed_file = "dataless.seed.BW_UH1.xml"
        >>> xml_str = open(xseed_file).read()  # doctest: +SKIP
        >>> c.station.put_resource(xseed_file, xml_str)  # doctest: +SKIP
        (201, 'OK')
        r   �xmlr   )rR   rr   rj   ��joinr|   r<   r�   r�   rT   )rG   r+   rr   rj   rb   r(   r(   r)   �put_resource'  s     �   �z_BaseRESTClient.put_resourcec                 C   s.   d� | jjd| j| j|g�}| jj|d|d�S )a;  
        DELETEs a XML resource.

        :type resource_name: str
        :param resource_name: Name of the resource.
        :type headers: dict
        :param headers: Header information for request,
            e.g. ``{'User-Agent': "obspyck"}``
        :return: (HTTP status code, HTTP status message)
        r   r�   r   )rR   rj   r�   )rG   r+   rj   rb   r(   r(   r)   �delete_resourceB  s     �  �z_BaseRESTClient.delete_resource)N)rv   rw   rx   rI   r�   r�   r�   r�   r(   r(   r(   r)   rz     s
   
rz   c                   @   sf   e Zd ZdZdd� Zdd� Zddd�Zdd	d
�Zddd�Zddd�Z	ddd�Z
ddd�Zddd�ZdS )r=   ah  
    Interface to access the SeisHub Waveform Web service.

    .. warning::
        This function should NOT be initialized directly, instead access the
        object via the :attr:`obspy.clients.seishub.Client.waveform` attribute.

    .. seealso:: https://github.com/barsch/seishub.plugins.seismology/blob/master/seishub/plugins/seismology/waveform.py
    c                 C   s
   || _ d S rt   r{   r}   r(   r(   r)   rI   ^  s    z_WaveformMapperClient.__init__c                 K   s&   d}| j j|f|�}dd� |�� D �S )zt
        Gets a list of network ids.

        :rtype: list
        :return: List of containing network ids.
        z"/seismology/waveform/getNetworkIdsc                 S   s   g | ]}t |d  ��qS )r   �r^   ��.0r3   r(   r(   r)   �
<listcomp>j  s     z9_WaveformMapperClient.get_network_ids.<locals>.<listcomp>)r|   ru   r,   )rG   rd   rb   r1   r(   r(   r)   �get_network_idsa  s    z%_WaveformMapperClient.get_network_idsNc                 K   sJ   t � �� D ]\}}|dkr
|||< q
d}| jj|f|�}dd� |�� D �S )z�
        Gets a list of station ids.

        :type network: str
        :param network: Network code, e.g. ``'BW'``.
        :rtype: list
        :return: List of containing station ids.
        r~   z"/seismology/waveform/getStationIdsc                 S   s   g | ]}t |d  ��qS )r   r�   r�   r(   r(   r)   r�   {  s     z9_WaveformMapperClient.get_station_ids.<locals>.<listcomp>�r�   r.   r|   ru   r,   )rG   r   rd   re   rf   rb   r1   r(   r(   r)   �get_station_idsl  s    

z%_WaveformMapperClient.get_station_idsc                 K   sJ   t � �� D ]\}}|dkr
|||< q
d}| jj|f|�}dd� |�� D �S )a  
        Gets a list of location ids.

        :type network: str
        :param network: Network code, e.g. ``'BW'``.
        :type station: str
        :param station: Station code, e.g. ``'MANZ'``.
        :rtype: list
        :return: List of containing location ids.
        r~   z#/seismology/waveform/getLocationIdsc                 S   s   g | ]}t |d  ��qS )r   r�   r�   r(   r(   r)   r�   �  s     z:_WaveformMapperClient.get_location_ids.<locals>.<listcomp>r�   �rG   r   r   rd   re   rf   rb   r1   r(   r(   r)   �get_location_ids}  s    
z&_WaveformMapperClient.get_location_idsc           	      K   sJ   t � �� D ]\}}|dkr
|||< q
d}| jj|f|�}dd� |�� D �S )ai  
        Gets a list of channel ids.

        :type network: str
        :param network: Network code, e.g. ``'BW'``.
        :type station: str
        :param station: Station code, e.g. ``'MANZ'``.
        :type location: str
        :param location: Location code, e.g. ``'00'``.
        :rtype: list
        :return: List of containing channel ids.
        r~   z"/seismology/waveform/getChannelIdsc                 S   s   g | ]}t |d  ��qS )r    r�   r�   r(   r(   r)   r�   �  s     z9_WaveformMapperClient.get_channel_ids.<locals>.<listcomp>r�   )	rG   r   r   r   rd   re   rf   rb   r1   r(   r(   r)   �get_channel_ids�  s    
z%_WaveformMapperClient.get_channel_idsc           
      K   s@   t � �� D ]\}}|dkr
|||< q
d}| jj|f|�}	t|	�S )a�  
        Gets a list of network latency values.

        :type network: str
        :param network: Network code, e.g. ``'BW'``.
        :type station: str
        :param station: Station code, e.g. ``'MANZ'``.
        :type location: str
        :param location: Location code, e.g. ``'00'``.
        :type channel: str
        :param channel: Channel code, e.g. ``'EHE'``.
        :rtype: list
        :return: List of dictionaries containing latency information.
        r~   z/seismology/waveform/getLatency�r�   r.   r|   ru   r6   )
rG   r   r   r   r    rd   re   rf   rb   r1   r(   r(   r)   �get_latency�  s    
z!_WaveformMapperClient.get_latencyFTc                 K   s�  t � �� D ]\}}|dkr
|||< q
dD ]&}t|| ttf�r(t|| �||< q(|d }|d }dt|d � }|| |d< || |d< d}| jj|f|�}|s�t	d��t
|�}t|�d	kr�t	d��|��  |r�|�||� |�r:|D ]R}| jjj|j|d
�}|
�r.| jjj|j|d
�}||k�r.d}t	|��||jd< q�|	�r�| jjj||||d�}|
�r�| jjj||||d�}||k�r�d}t	|��|D ]}|�� |jd< �q�|S )a�  
        Gets a ObsPy Stream object.

        :type network: str
        :param network: Network code, e.g. ``'BW'``.
        :type station: str
        :param station: Station code, e.g. ``'MANZ'``.
        :type location: str
        :param location: Location code, e.g. ``'00'``.
        :type channel: str
        :param channel: Channel code, supporting wildcard for component,
            e.g. ``'EHE'`` or ``'EH*'``.
        :type starttime: :class:`~obspy.core.utcdatetime.UTCDateTime`
        :param starttime: Start date and time.
        :type endtime: :class:`~obspy.core.utcdatetime.UTCDateTime`
        :param endtime: End date and time.
        :type apply_filter: bool, optional
        :param apply_filter: Apply filter (default is ``False``).
        :type get_paz: bool, optional
        :param get_paz: Fetch PAZ information and append to
            :class:`~obspy.core.trace.Stats` of all fetched traces. This
            considerably slows down the request (default is ``False``).
        :type get_coordinates: bool, optional
        :param get_coordinates: Fetch coordinate information and append to
            :class:`~obspy.core.trace.Stats` of all fetched traces. This
            considerably slows down the request (default is ``False``).
        :type metadata_timecheck: bool, optional
        :param metadata_timecheck: For ``get_paz`` and ``get_coordinates``
            check if metadata information is changing from start to end
            time. Raises an Exception if this is the case. This can be
            deactivated to save time.
        :rtype: :class:`~obspy.core.stream.Stream`
        :return: A ObsPy Stream object.
        r~   )r!   r"   r!   r"   r   r    z /seismology/waveform/getWaveform�No waveform data availabler   ��seed_idr	   z5PAZ information changing from start time to end time.�paz)r   r   r   r	   z<Coordinate information changing from start time to end time.�coordinates)r�   r.   r[   r^   r   r   r   r|   ri   rN   r*   r]   Z_cleanupZtrimr   �get_paz�idZstats�get_coordinates�copy)rG   r   r   r   r    r!   r"   Zapply_filterr�   r�   Zmetadata_timecheckrd   re   rf   Ztime_Z
trim_startZtrim_endZdeltarb   r&   �streamZtrr�   Z	paz_checkrq   �coordsZcoords_checkr(   r(   r)   �get_waveforms�  sl    (
��
  �  �
z#_WaveformMapperClient.get_waveformsc                 K   sP   t � �� D ]\}	}
|	dkr
|
||	< q
d}| jj|f|�}|sDtd��t|�}|S )a  
        Gets a preview of a ObsPy Stream object.

        :type network: str
        :param network: Network code, e.g. ``'BW'``.
        :type station: str
        :param station: Station code, e.g. ``'MANZ'``.
        :type location: str
        :param location: Location code, e.g. ``'00'``.
        :type channel: str
        :param channel: Channel code, supporting wildcard for component,
            e.g. ``'EHE'`` or ``'EH*'``.
        :type starttime: :class:`~obspy.core.utcdatetime.UTCDateTime`
        :param starttime: Start date and time.
        :type endtime: :class:`~obspy.core.utcdatetime.UTCDateTime`
        :param endtime: End date and time.
        :rtype: :class:`~obspy.core.stream.Stream`
        :return: Waveform preview as ObsPy Stream object.
        r~   �/seismology/waveform/getPreviewr�   )r�   r.   r|   ri   rN   r*   )rG   r   r   r   r    r!   r"   �	trace_idsrd   re   rf   rb   r&   r�   r(   r(   r)   �get_previews#  s    
z"_WaveformMapperClient.get_previewsc           
      K   sx   t � �� D ]\}}|dkr
|||< q
d|krLt|d t�rLd�|d �|d< d}| jj|f|�}|sltd��t|�}	|	S )a�  
        Gets a preview of a ObsPy Stream object.

        :type trace_ids: list
        :param trace_ids: List of trace IDs, e.g. ``['BW.MANZ..EHE']``.
        :type starttime: :class:`~obspy.core.utcdatetime.UTCDateTime`
        :param starttime: Start date and time.
        :type endtime: :class:`~obspy.core.utcdatetime.UTCDateTime`
        :param endtime: End date and time.
        :rtype: :class:`~obspy.core.stream.Stream`
        :return: Waveform preview as ObsPy Stream object.
        r~   r�   �,r�   r�   )	r�   r.   r[   r_   r�   r|   ri   rN   r*   )
rG   r�   r!   r"   rd   re   rf   rb   r&   r�   r(   r(   r)   �get_previews_by_idsE  s    
z)_WaveformMapperClient.get_previews_by_ids)N)NN)NNN)NNNN)NNNNNFFT)NNNNN)NNN)rv   rw   rx   ry   rI   r�   r�   r�   r�   r�   r�   r�   r�   r(   r(   r(   r)   r=   S  s.   



  �
          �
e      �
"r=   c                   @   s4   e Zd ZdZdZdZddd�Zddd	�Zd
d� ZdS )r>   af  
    Interface to access the SeisHub Station Web service.

    .. warning::
        This function should NOT be initialized directly, instead access the
        object via the :attr:`obspy.clients.seishub.Client.station` attribute.

    .. seealso:: https://github.com/barsch/seishub.plugins.seismology/blob/master/seishub/plugins/seismology/waveform.py
    �
seismologyr   Nc                 K   s@   t � �� D ]\}}|dkr
|||< q
d}| jj|f|�}t|�S )a3  
        Gets a list of station information.

        :type network: str
        :param network: Network code, e.g. ``'BW'``.
        :type station: str
        :param station: Station code, e.g. ``'MANZ'``.
        :rtype: list
        :return: List of dictionaries containing station information.
        r~   z/seismology/station/getListr�   r�   r(   r(   r)   �get_lists  s    
z_StationMapperClient.get_list� c                 C   s>  i }t � �� D ]\}}|dkr|||< qd�||g�}| jj�|g �D ]d}	|	d dkrXn|t|	d �k rjqF|	d dkrxn|t|	d �kr�qFi }
dD ]}|	| |
|< q�|
  S | jf |�}|s�d|||f }t|��| jj�	|g �}|D ]}	|	|kr�|�
|	� q�t|�dk�rt�d	� |d
 }i }
dD ]}|| |
|< �q&|
S )a�  
        Get coordinate information.

        Returns a dictionary with coordinate information for specified station
        at the specified time.

        :type network: str
        :param network: Network code, e.g. ``'BW'``.
        :type station: str
        :param station: Station code, e.g. ``'MANZ'``.
        :type datetime: :class:`~obspy.core.utcdatetime.UTCDateTime`
        :param datetime: Time for which the PAZ is requested,
            e.g. ``'2010-01-01 12:00:00'``.
        :type location: str
        :param location: Location code, e.g. ``'00'``.
        :rtype: dict
        :return: Dictionary containing station coordinate information.
        r~   �.r   r�   r   )�latitude�	longitudeZ	elevationz&No coordinates for station %s.%s at %srW   z1Received more than one metadata set. Using first.r   )r�   r.   r�   r|   rE   �getr   r�   rN   �
setdefaultr0   r]   �warnings�warn)rG   r   r   r	   r   rd   re   rf   Znetstar&   r�   Zmetadatarq   Zstalistr(   r(   r)   r�   �  sF    
�
z$_StationMapperClient.get_coordinatesc                 C   sV  | j j�|g �D ]B}t|�}z|j|t|�d�}|W   S  tk
rP   Y qY qX q|�d�\}}}}	| j|||d�}
|
s~i S dD ]}||kr�d}t	|��q�|
D ]�}| j j
�|d �}| j j�|g �}||kr�|�|� t|�}z|j|t|�d�}W nB tk
�r8 } z"d}t|��|��r&W Y �q�� W 5 d}~X Y nX  �qRq�d	| }t|��|S )
a�  
        Get PAZ for a station at given time span. Gain is the A0 normalization
        constant for the poles and zeros.

        :type seed_id: str
        :param seed_id: SEED or channel id, e.g. ``"BW.RJOB..EHZ"`` or
            ``"EHE"``.
        :type datetime: :class:`~obspy.core.utcdatetime.UTCDateTime`
        :param datetime: Time for which the PAZ is requested,
            e.g. ``'2010-01-01 12:00:00'``.
        :rtype: dict
        :return: Dictionary containing zeros, poles, gain and sensitivity.

        .. rubric:: Example

        >>> c = Client(timeout=20)
        >>> paz = c.station.get_paz(
        ...     'BW.MANZ..EHZ', '20090707')  # doctest: +SKIP
        >>> paz['zeros']  # doctest: +SKIP
        [0j, 0j]
        >>> len(paz['poles'])  # doctest: +SKIP
        5
        >>> print(paz['poles'][0])  # doctest: +SKIP
        (-0.037004+0.037016j)
        >>> paz['gain']  # doctest: +SKIP
        60077000.0
        >>> paz['sensitivity']  # doctest: +SKIP
        2516800000.0
        r�   r�   )r   r   r	   )r   rX   z%Wildcards in seed_id are not allowed.r+   z(No channel found with the given SEED id:Nz+No channel found with the given SEED id: %s)r|   rD   r�   r   r�   r   rN   �splitr�   rl   r   r�   r�   r0   r   r^   �
startswith)rG   r�   r	   �res�parserr�   r   r   r   r    rE   Zwildcardrq   Zxml_docZreslistrs   Znot_found_msgr(   r(   r)   r�   �  sL    �
�

�
z_StationMapperClient.get_paz)NN)r�   )	rv   rw   rx   ry   r�   r�   r�   r�   r�   r(   r(   r(   r)   r>   e  s   


<r>   c                   @   sL   e Zd ZdZdZdZeddd��ddd��Zd	d
� Zddd�Z	ddd�Z
dS )r?   a_  
    Interface to access the SeisHub Event Web service.

    .. warning::
        This function should NOT be initialized directly, instead access the
        object via the :attr:`obspy.clients.seishub.Client.event` attribute.

    .. seealso:: https://github.com/barsch/seishub.plugins.seismology/blob/master/seishub/plugins/seismology/event.py
    r�   r@   N)Z
first_pickZ	last_pick�2   c           !      K   s�   |dkrd}t |��t� �� D ]\}}|dkr|||< qd}| jj|f|�}t|�} |t| �ks�|dkrtt| �dks�t| �dkr�d}t�|� | S )a3  
        Gets a list of event information.

        ..note:
            For SeisHub versions < 1.4 available keys include "user" and
            "account". In newer SeisHub versions they are replaced by "author".

        :rtype: list
        :return: List of dictionaries containing event information.

        The number of resulting events is by default limited to 50 entries from
        a SeisHub server. You may raise this by setting the ``limit`` option to
        a maximal value of 2500. Numbers above 2500 will result into an
        exception.
        i�	  z&Maximal allowed limit is 2500 entries.r~   z/seismology/event/getListNr�   z:List of results might be incomplete due to option 'limit'.)	rl   r�   r.   r|   ru   r6   r]   r�   r�   )!rG   �limit�offset�localisation_methodZauthorZmin_datetimeZmax_datetimeZmin_first_pickZmax_first_pickZmin_last_pickZmax_last_pickZmin_latitudeZmax_latitudeZmin_longitudeZmax_longitudeZmin_magnitudeZmax_magnitudeZ	min_depthZ	max_depth�used_pZ
min_used_pZ
max_used_p�used_sZ
min_used_sZ
max_used_sZdocument_idrd   rq   re   rf   rb   r1   Zresultsr(   r(   r)   r�     s&    
�
�
�
z_EventMapperClient.get_listc                 K   s>   dd� | j f |�D �}t� }|D ]}|�t| �|��� q |S )a�  
        Fetches a catalog with event information. Parameters to narrow down
        the request are the same as for :meth:`get_list`.

        .. warning::
            Only works when connecting to a SeisHub server of version 1.4.0
            or higher (serving event data as QuakeML).

        :rtype: :class:`~obspy.core.event.Catalog`
        :returns: Catalog containing event information matching the request.

        The number of resulting events is by default limited to 50 entries from
        a SeisHub server. You may raise this by setting the ``limit`` option to
        a maximal value of 2500. Numbers above 2500 will result into an
        exception.
        c                 S   s   g | ]}|d  �qS )r+   r(   )r�   �itemr(   r(   r)   r�   U  s   �z1_EventMapperClient.get_events.<locals>.<listcomp>)r�   r   �extendr   r�   )rG   rd   Zresource_names�catr+   r(   r(   r)   �
get_eventsD  s    
�z_EventMapperClient.get_eventsFc                 K   s  | j f |�}t�� }td�}|�dd� t|d�}dt|d�_t|d�}|�dd	� t|d
�}dt|d�_t|d�}	dt|	d�_t|d�}
|
�dd� |
�dd� |
�dd� |
�dd� t|d�}dt|d�_dt|d�_t|d�}d|��  t|d�_dt|d�_d| jj	 }|d | 7 }|d!7 }|d"�
d#d$� |�� D ��7 }|t|d%�_t|d�}t|d&�}d't|d(�_d)t|d*�_d+t|d,�_d-d.d/d0d1d2d3d4d5d6d7d8g}|D �]d}t|d9�}t|d2 ��d:�d; }t|d6 �}|�r$t|�}d<||f }zd=td>| � }W n tk
�r    d?}Y nX n|}d@}|�r@dAt|d�_n|t|d�_dBt|dC�_t|d�}t|d
�}t|d&�}d+t|d,�_t|�t|d�_|d3 �r�|d4 �r�t|dD�}dE|d3 |d4 f t|dF�_dA}|D ](}||k�r�q�|dG||| f 7 }�q�|t|d%�_�q�t|dHdHdI�S )Ja  
        Posts an event.get_list() and returns the results as a KML file. For
        optional arguments, see documentation of
        :meth:`~obspy.clients.seishub.client._EventMapperClient.get_list()`

        :type nolabels: bool
        :param nolabels: Hide labels of events in KML. Can be useful with large
            data sets.
        :rtype: str
        :return: String containing KML information of all matching events. This
            string can be written to a file and loaded into e.g. Google Earth.
        �kmlZxmlnszhttp://www.opengis.net/kml/2.2ZDocumentzSeisHub Event Locations�nameZStyler�   Z
earthquakeZ	IconStylez0.5ZscaleZIconz:https://maps.google.com/mapfiles/kml/shapes/earthquake.pngZhrefZhotSpot�x�y�0ZxunitsZfractionZyunitsZ
LabelStyleZff0000ffZcolorz0.8ZFolderzSeisHub Events (%s)�1�openzFetched from: %sz
Fetched at: %sz

Search options:
�
c                 S   s&   g | ]\}}d � t|�t|�f��qS )�=)r�   r^   )r�   r4   r5   r(   r(   r)   r�   �  s   �z._EventMapperClient.get_kml.<locals>.<listcomp>ZdescriptionZ	ListStyleZcheckZlistItemTypeZ00ffffffZbgColor�5ZmaxSnippetLinesr+   r�   ZaccountrH   Zpublicr	   r�   r�   ZdepthZ	magnituder�   r�   Z	Placemark� r   z%s: %.1fg333333�?g      �?g�������?g      �?r�   z#earthquakeZstyleUrlZPointz%.10f,%.10f,0r�   z
%s: %sT)Zpretty_printZxml_declaration)r�   r	   Znowr   �setr   r/   �dater|   r<   r�   r.   r^   r�   �floatr
   rl   r   )rG   Znolabelsrd   ZeventsZ	timestampr�   ZdocumentZstyleZ	iconstyleZiconZhotspotZ
labelstyleZfolderZdescrip_strZ	liststyleZinteresting_keysZ
event_dictZ	placemarkr�   ZmagZlabelZ	icon_sizeZ
icon_styleZpointre   r(   r(   r)   �get_kml\  s�    



�



��


      �





�
z_EventMapperClient.get_kmlc                 K   s<   |st j�|�rtd| ��| jf |�}t|d��|� dS )a=  
        Posts an event.get_list() and writes the results as a KML file. For
        optional arguments, see help for
        :meth:`~obspy.clients.seishub.client._EventMapperClient.get_list()` and
        :meth:`~obspy.clients.seishub.client._EventMapperClient.get_kml()`

        :type filename: str
        :param filename: Filename (complete path) to save KML to.
        :type overwrite: bool
        :param overwrite: Overwrite existing file, otherwise if file exists an
            Exception is raised.
        :type nolabels: bool
        :param nolabels: Hide labels of events in KML. Can be useful with large
            data sets.
        :rtype: str
        :return: String containing KML information of all matching events. This
            string can be written to a file and loaded into e.g. Google Earth.
        z#File %s exists and overwrite=False.ZwtN)�os�path�lexists�OSErrorr�   r�   �write)rG   �filenameZ	overwriterd   Z
kml_stringr(   r(   r)   �save_kml�  s
    z_EventMapperClient.save_kml)r�   NNNNNNNNNNNNNNNNNNNNNNNN)F)F)rv   rw   rx   ry   r�   r�   r   r�   r�   r�   r�   r(   r(   r(   r)   r?   
  sD   
 �                                     �*
lr?   c                   @   s    e Zd ZdZdd� Zdd� ZdS )rp   z�
    Improved urllib2.Request Class for which the HTTP Method can be set to
    values other than only GET and POST.
    See http://benjamin.smedbergs.us/blog/2008-10-21/    putting-and-deleteing-in-python-urllib2/
    c                 O   s:   |t krddt   }t|��tjj| f|�|� || _d S )NzHTTP Method not supported. zSupported are: %s.)rk   rl   rF   �RequestrI   �_method)rG   rR   rc   rd   rq   r(   r(   r)   rI   �  s    �z_RequestWithMethod.__init__c                 C   s   | j S rt   )r�   )rG   r(   r(   r)   �
get_method�  s    z_RequestWithMethod.get_methodN)rv   rw   rx   ry   rI   r�   r(   r(   r(   r)   rp   �  s   rp   �__main__T)Zexclude_empty)>ry   Z
__future__r   r   r   r   Zfuture.builtinsZfuture.utilsr   r   r�   r$   �sysrK   r�   r	   Zmathr
   �version_info�majorZurllibr   Zurllib2rF   Zurllib.parseZurllib.requestZrequestZlxmlr   Z
lxml.etreer   r   r   Zobspyr   r   r   Zobspy.core.utilr   Zobspy.core.util.decoratorr   Zobspy.io.xseedr   Zobspy.io.xseed.utilsr   rm   ro   rk   rY   r*   r6   �objectr7   rz   r=   r>   r?   r�   rp   rv   ZdoctestZtestmodr(   r(   r(   r)   �<module>   sb   	

�  � .N   & Y
